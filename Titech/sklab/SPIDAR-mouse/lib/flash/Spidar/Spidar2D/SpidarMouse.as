﻿package Spidar.Spidar2D {
    import flash.display.Sprite;
    import flash.events.*;
	// XML通信用ソケット
    import flash.net.XMLSocket;
	import flash.text.TextField;
	import flash.utils.*;

    public class SpidarMouse extends Sprite {
		// XMLソケット
        private var socket:XMLSocket;
		// テキストデータ
		private var tf:TextField = new TextField();
		// サーバから受信したデバイスの座標
		private var position:Array = [0, 0, 0];
		// Flashコンテンツ内のポインタの座標
		private var pPos:Array = [0, 0, 0];
		// ポート番号
		private var portNum:Number = 8080;
		private var host:String = "";

/*
		// コンストラクタ
        public function SpidarMouse() {
			// XMLソケットの作成
            socket = new XMLSocket();
			// ソケットの設定
            configureListeners(socket);
			// ソケットへの接続
            socket.connect("localhost", 8080);
			// data
			portNum = 8080;
			host = "localhost";
        }
*/
		
		// コンストラクタ（ホスト名:"localhost"、ポート番号:8080）
        public function SpidarMouse(hostName:String, port:uint) {
			// XMLソケットの作成
            socket = new XMLSocket();
			// ソケットの設定
            configureListeners(socket);
			// ソケットへの接続
            socket.connect(hostName, port);
			// data
			portNum = port;
			host = hostName;
        }

		// サーバへデータを送信
        private function send(data:Object):void {
			// 引数の文字列データをサーバへ送信
            socket.send(data);
        }

		// ソケットの設定
        private function configureListeners(dispatcher:IEventDispatcher):void {
			// イベントリスナの登録
            dispatcher.addEventListener(Event.CLOSE, closeHandler);
            dispatcher.addEventListener(Event.CONNECT, connectHandler);
            dispatcher.addEventListener(DataEvent.DATA, dataHandler);
            dispatcher.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
            dispatcher.addEventListener(ProgressEvent.PROGRESS, progressHandler);
            dispatcher.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
        }

		// デバイスへの力の出力を行う関数（力の強さ（0.0~2.0）、力の方向単位ベクトル）
		public function setForce(fx:Number, fy:Number, ms:Number): void {
			// 3つのデータをサーバへ送信
			tf.text = fx + "," + fy + "," + ms;
			send(tf.text);
		}

		// ソケットが閉じられたときに呼ばれる関数
        private function closeHandler(event:Event):void {
            trace("closeHandler: " + event);
			tf.text = "closeHandler: " + event;
			addChild(tf);
		}

		// ソケットが接続したときに呼ばれる関数
        private function connectHandler(event:Event):void {
            trace("connectHandler: " + event);
//			tf.text = "connectHandler: " + event;
//			addChild(tf);
        }

		// サーバからデータを受信したときに呼ばれる関数
        private function dataHandler(event:DataEvent):void {
			// サーバからデバイスの位置情報が送られてくるので変数へ格納する
            trace("dataHandler: " + event);
			var list:Array = event.data.split(",");
			position = [parseFloat(list[0]), parseFloat(list[1]), parseFloat(list[2]), parseInt(list[3])];

			// サーバへポインタの位置を返す
			var sendData:String = "0,0,0";
			send(sendData);
			tf.text = "dataHandler: " + event;
			addChild(tf);
			
			if (position[2] == 8080) {
				tf.text = "closeScoket!";
				addChild(tf);				
				// ソケットへの接続
				portNum = portNum + position[1] + 1;
				socket.close();
				tf.text = "reconnect: " + host + ": " + portNum;
				addChild(tf);
				socket.connect(host, portNum);
			}
        }

		// I/Oにエラーがあった場合に呼び出される関数
        private function ioErrorHandler(event:IOErrorEvent):void {
            trace("ioErrorHandler: " + event);
			tf.text = "ioErrorHandler: " + event;
			addChild(tf);
        }

		// データ受信中に呼ばれる関数
        private function progressHandler(event:ProgressEvent):void {
            trace("progressHandler loaded:" + event.bytesLoaded + " total: " + event.bytesTotal);
			tf.text = "progressHandler loaded:" + event.bytesLoaded + " total: " + event.bytesTotal;
			addChild(tf);
        }

		// セキュリティエラーが発生した場合に呼ばれる関数
        private function securityErrorHandler(event:SecurityErrorEvent):void {
            trace("securityErrorHandler: " + event);
			tf.text = "securityErrorHandler: " + event;
			addChild(tf);
        }
    }
}