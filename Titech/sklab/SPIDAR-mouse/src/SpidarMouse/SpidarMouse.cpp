// Spidar2D.cpp : Defines the exported functions for the DLL application.
//


#include "SpidarMouse.h"
#include <stdio.h>

#include <Windows.h>	//Definitions for various common and not so common types like DWORD, PCHAR, HANDLE, etc.
#include <setupapi.h>	//From Platform SDK. Definitions needed for the SetupDixxx() functions, which we use to
						//find our plug and play device.

#include   <stdlib.h>   
#include <winuser.h>
#include <math.h>

#include<mmsystem.h>
#pragma comment(lib, "winmm.lib")


//Modify this value to match the VID and PID in your USB device descriptor.
//Use the formatting: "Vid_xxxx&Pid_xxxx" where xxxx is a 16-bit hexadecimal number.
//#define MY_DEVICE_ID  "Vid_04d8&Pid_003F"	
//#define MY_DEVICE_ID  "HID\VID_04B3&PID_3107&REV_0130"
wchar_t* DeviceIDToFind = L"VID_04D8&PID_003F";



//  Variables that need to have wide scope.
HANDLE WriteHandle = INVALID_HANDLE_VALUE;	//Need to get a write "handle" to our device before we can write to it.
HANDLE ReadHandle = INVALID_HANDLE_VALUE;	//Need to get a read "handle" to our device before we can read from it.
HANDLE WriteHandle2 = INVALID_HANDLE_VALUE;	//Need to get a write "handle" to our device before we can write to it.
HANDLE ReadHandle2 = INVALID_HANDLE_VALUE;	//Need to get a read "handle" to our device before we can read from it.
int deviceNum = 0;
//the following are used to set multimedia timer from control at required frequency
UINT uDelay = 4;//frequency=1000/uDelay 
UINT uResolution = 1;
DWORD dwUser = NULL;
UINT fuEvent = TIME_PERIODIC; //You also choose TIME_ONESHOT;
MMRESULT FTimerID_Control;



float interG_X=0.0;
float interG_Y=0.0;
float SetForce_Mag=0.0;
float SetForce_Dirunit_X=0.0;
float SetForce_Dirunit_Y=0.0;

float Max_Force=1.5;

unsigned char OutputPacketBuffer[65];	//Allocate a memory buffer equal to our endpoint size + 1
unsigned char InputPacketBuffer[65];	//Allocate a memory buffer equal to our endpoint size + 1

unsigned char *MinForce_Bytes;
unsigned char *MaxForce_Bytes;
//the following is used for one-pointer operating mode 
unsigned char *interG_X_Bytes;
unsigned char *interG_Y_Bytes;
unsigned char *Force_Mag_Bytes;
unsigned char *Force_Dirunit_X_Bytes;
unsigned char *Force_Dirunit_Y_Bytes;
//the following is used to calculate duty value from required force
unsigned char *Fun_a_Bytes;
unsigned char *Fun_b_Bytes;

unsigned char duty1value,duty2value, duty3value, duty4value;

bool impulseFlag = false;
bool continuousFlag=false;
bool isSetDutyOnCh= false;
LONG nowTime = 0, waitTime=0;

DWORD BytesWritten = 0;
DWORD BytesRead = 0;

//the following is for configuration data
char conf_filename[256];
char conf_buffer[256]; // Write data
float conf_data[100];// Read data


void CALLBACK TimeProcControl(UINT uTimerID , UINT uMsg , DWORD dwUser, DWORD dw1, DWORD dw2)
{

	if (!continuousFlag)
	{
		if (impulseFlag)
		{
			nowTime+=uDelay;
			if (nowTime >= waitTime)
			{
				if(isSetDutyOnCh)
				{
					duty1value=0;
					duty2value=0;
					duty3value=0;
					duty4value=0;
				}
				else
				{
					SetForce_Mag=0;
				}
				impulseFlag = false;
			}
		}
	}
	
	if (isSetDutyOnCh) 
	{
		 OutputPacketBuffer[0] = 0;	
			
		 OutputPacketBuffer[1] = 0x93;		 
		
		 OutputPacketBuffer[2] = duty1value;
		 OutputPacketBuffer[3] = duty2value;
		 OutputPacketBuffer[4] = duty3value;
		 OutputPacketBuffer[5] = duty4value;

	}
	else
	{
		 
		 OutputPacketBuffer[0] = 0;			//The first byte is the "Report ID".  This number is used by the USB driver, but does not
										//get tranmitted accross the USB bus.  The custom HID class firmware is only configured for
										//one type of report, therefore, we must always initializate this byte to "0" before sending
										//a data packet to the device.

			
		 OutputPacketBuffer[1] = 0x97;		 

		 interG_X_Bytes = (unsigned char *)&interG_X;
		 interG_Y_Bytes = (unsigned char *)&interG_Y;
		 Force_Mag_Bytes = (unsigned char *)&SetForce_Mag;
		 Force_Dirunit_X_Bytes = (unsigned char *)&SetForce_Dirunit_X;
		 Force_Dirunit_Y_Bytes = (unsigned char *)&SetForce_Dirunit_Y;
		
		 OutputPacketBuffer[2] = interG_X_Bytes[0];
		 OutputPacketBuffer[3] = interG_X_Bytes[1];
		 OutputPacketBuffer[4] = interG_X_Bytes[2];
		 OutputPacketBuffer[5] = interG_X_Bytes[3];

		 OutputPacketBuffer[6] = interG_Y_Bytes[0];
		 OutputPacketBuffer[7] = interG_Y_Bytes[1];
		 OutputPacketBuffer[8] = interG_Y_Bytes[2];
		 OutputPacketBuffer[9] = interG_Y_Bytes[3];

		 OutputPacketBuffer[10] = Force_Mag_Bytes[0] ;
		 OutputPacketBuffer[11] = Force_Mag_Bytes[1] ;
		 OutputPacketBuffer[12] = Force_Mag_Bytes[2] ;
		 OutputPacketBuffer[13] = Force_Mag_Bytes[3] ;

		 OutputPacketBuffer[14] = Force_Dirunit_X_Bytes[0] ;
		 OutputPacketBuffer[15] = Force_Dirunit_X_Bytes[1] ;
		 OutputPacketBuffer[16] = Force_Dirunit_X_Bytes[2] ;
		 OutputPacketBuffer[17] = Force_Dirunit_X_Bytes[3] ;

		 OutputPacketBuffer[18] = Force_Dirunit_Y_Bytes[0] ;
		 OutputPacketBuffer[19] = Force_Dirunit_Y_Bytes[1] ;
		 OutputPacketBuffer[20] = Force_Dirunit_Y_Bytes[2] ;
		 OutputPacketBuffer[21] = Force_Dirunit_Y_Bytes[3] ;

	}
	

	WriteFile(WriteHandle, &OutputPacketBuffer, 65, &BytesWritten, 0);	//Blocking function, unless an "overlapped" structure is used
					
				
}

int OpenSpidarMouse()
{

	GUID InterfaceClassGuid = {0x4d1e55b2, 0xf16f, 0x11cf, 0x88, 0xcb, 0x00, 0x11, 0x11, 0x00, 0x00, 0x30}; 

	HDEVINFO DeviceInfoTable = INVALID_HANDLE_VALUE;
	PSP_DEVICE_INTERFACE_DATA InterfaceDataStructure = new SP_DEVICE_INTERFACE_DATA;
	PSP_DEVICE_INTERFACE_DETAIL_DATA DetailedInterfaceDataStructure = new SP_DEVICE_INTERFACE_DETAIL_DATA;
	SP_DEVINFO_DATA DevInfoData;

	DWORD InterfaceIndex = 0;
	DWORD StatusLastError = 0;
	DWORD dwRegType;
	DWORD dwRegSize;
	DWORD StructureSize = 0;
	PBYTE PropertyValueBuffer;
	bool MatchFound = false;
	DWORD ErrorStatus;

	//	char*   DeviceIDToFind = MY_DEVICE_ID;

	//First populate a list of plugged in devices (by specifying "DIGCF_PRESENT"), which are of the specified class GUID. 
	DeviceInfoTable = SetupDiGetClassDevs(&InterfaceClassGuid, NULL, NULL, DIGCF_PRESENT | DIGCF_DEVICEINTERFACE);

	//Now look through the list we just populated.  We are trying to see if any of them match our device. 
	while(true)
	{
		InterfaceDataStructure->cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
		SetupDiEnumDeviceInterfaces(DeviceInfoTable, NULL, &InterfaceClassGuid, InterfaceIndex, InterfaceDataStructure);
		ErrorStatus = GetLastError();
		if(ERROR_NO_MORE_ITEMS == GetLastError())	//Did we reach the end of the list of matching devices in the DeviceInfoTable?
		{	//Cound not find the device.  Must not have been attached.
			SetupDiDestroyDeviceInfoList(DeviceInfoTable);	//Clean up the old structure we no longer need.
			return -1;		
		}

		//Now retrieve the hardware ID from the registry.  The hardware ID contains the VID and PID, which we will then 
		//check to see if it is the correct device or not.

		//Initialize an appropriate SP_DEVINFO_DATA structure.  We need this structure for SetupDiGetDeviceRegistryProperty().
		DevInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
		SetupDiEnumDeviceInfo(DeviceInfoTable, InterfaceIndex, &DevInfoData);

		//First query for the size of the hardware ID, so we can know how big a buffer to allocate for the data.
		SetupDiGetDeviceRegistryProperty(DeviceInfoTable, &DevInfoData, SPDRP_HARDWAREID, &dwRegType, NULL, 0, &dwRegSize);

		//Allocate a buffer for the hardware ID.
		PropertyValueBuffer = (BYTE *) malloc (dwRegSize);
		if(PropertyValueBuffer == NULL)	//if null, error, couldn't allocate enough memory
		{	//Can't really recover from this situation, just exit instead.
			SetupDiDestroyDeviceInfoList(DeviceInfoTable);	//Clean up the old structure we no longer need.
			return -2;		
		}

		//Retrieve the hardware IDs for the current device we are looking at.  PropertyValueBuffer gets filled with a 
		//REG_MULTI_SZ (array of null terminated strings).  To find a device, we only care about the very first string in the
		//buffer, which will be the "device ID".  The device ID is a string which contains the VID and PID, in the example 
		//format "Vid_04d8&Pid_003f".
		SetupDiGetDeviceRegistryProperty(DeviceInfoTable, &DevInfoData, SPDRP_HARDWAREID, &dwRegType, PropertyValueBuffer, dwRegSize, NULL);

		//Now check if the first string in the hardware ID matches the device ID of my USB device.
		//#ifdef UNICODE
		//String^ DeviceIDFromRegistry = gcnew String((wchar_t *)PropertyValueBuffer);
		//char* DeviceIDFromRegistry = (char*)PropertyValueBuffer;
		wchar_t* DeviceIDFromRegistry = ((wchar_t *)PropertyValueBuffer);
		//#else
		//String^ DeviceIDFromRegistry = gcnew String((char *)PropertyValueBuffer);
		//#endif
		//Convert both strings to lower case.  This makes the code more robust/portable accross OS Versions

		//DeviceIDFromRegistry =tolower(DeviceIDFromRegistry);	
		//DeviceIDToFind = DeviceIDToFind->ToLowerInvariant();				
		//Now check if the hardware ID we are looking at contains the correct VID/PID
		MatchFound = substr(DeviceIDFromRegistry,DeviceIDToFind);		
		if(MatchFound == true)
		{
			//Device must have been found.  Open read and write handles.  In order to do this, we will need the actual device path first.
			//We can get the path by calling SetupDiGetDeviceInterfaceDetail(), however, we have to call this function twice:  The first
			//time to get the size of the required structure/buffer to hold the detailed interface data, then a second time to actually 
			//get the structure (after we have allocated enough memory for the structure.)
			DetailedInterfaceDataStructure->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);
			//First call populates "StructureSize" with the correct value
			SetupDiGetDeviceInterfaceDetail(DeviceInfoTable, InterfaceDataStructure, NULL, NULL, &StructureSize, NULL);	
			DetailedInterfaceDataStructure = (PSP_DEVICE_INTERFACE_DETAIL_DATA)(malloc(StructureSize));		//Allocate enough memory
			if(DetailedInterfaceDataStructure == NULL)	//if null, error, couldn't allocate enough memory
			{	//Can't really recover from this situation, just exit instead.
				SetupDiDestroyDeviceInfoList(DeviceInfoTable);	//Clean up the old structure we no longer need.
				return -3;		
			}
			DetailedInterfaceDataStructure->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);
			//Now call SetupDiGetDeviceInterfaceDetail() a second time to receive the goods.  
			SetupDiGetDeviceInterfaceDetail(DeviceInfoTable, InterfaceDataStructure, DetailedInterfaceDataStructure, StructureSize, NULL, NULL); 

			//We now have the proper device path, and we can finally open read and write handles to the device.
			//We store the handles in the global variables "WriteHandle" and "ReadHandle", which we will use later to actually communicate.
			WriteHandle = CreateFile((DetailedInterfaceDataStructure->DevicePath), GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, 0);

			ErrorStatus = GetLastError();
			/*if(ErrorStatus == ERROR_SUCCESS)
			ToggleLED_btn->Enabled = true;			*/	//Make button no longer greyed out
			ReadHandle = CreateFile((DetailedInterfaceDataStructure->DevicePath), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, 0);
			ErrorStatus = GetLastError();
			if(ErrorStatus == ERROR_SUCCESS)
			{
				SetupDiDestroyDeviceInfoList(DeviceInfoTable);	//Clean up the old structure we no longer need.


				sprintf_s(conf_filename, 256, ".\\SpidarMouse.ini");

				GetPrivateProfileStringA("Force", "minForce", "0.1", conf_buffer, sizeof(conf_buffer), conf_filename);
				conf_data[0] = atof(conf_buffer);
				MinForce_Bytes = (unsigned char *)&conf_data[0];
				
                
				GetPrivateProfileStringA("Force", "maxForce", "1.5", conf_buffer, sizeof(conf_buffer), conf_filename);
				conf_data[1] = atof(conf_buffer);
				Max_Force=conf_data[1];
                MaxForce_Bytes = (unsigned char *)&conf_data[1];

				//duty=Fun_a*Force+Fun_b   
				GetPrivateProfileStringA("Function", "Fun_a", "47.9867", conf_buffer, sizeof(conf_buffer), conf_filename);
				conf_data[2] = atof(conf_buffer);
				Fun_a_Bytes = (unsigned char *)&conf_data[2];
				

				GetPrivateProfileStringA("Function", "Fun_b", "13.1215", conf_buffer, sizeof(conf_buffer), conf_filename);
				conf_data[3] = atof(conf_buffer);
                Fun_b_Bytes = (unsigned char *)&conf_data[3];

				DWORD BytesWritten = 0;

				unsigned char OutputPacketBuffer[65];	//Allocate a memory buffer equal to our endpoint size + 1


				OutputPacketBuffer[0] = 0;			//The first byte is the "Report ID".  This number is used by the USB driver, but does not
				//get tranmitted accross the USB bus.  The custom HID class firmware is only configured for
				//one type of report, therefore, we must always initializate this byte to "0" before sending
				//a data packet to the device.

				OutputPacketBuffer[1] = 0x80;		//0x80 is used to initialize the device

				OutputPacketBuffer[2] = MinForce_Bytes[0];
				OutputPacketBuffer[3] = MinForce_Bytes[1];
				OutputPacketBuffer[4] = MinForce_Bytes[2];
				OutputPacketBuffer[5] = MinForce_Bytes[3];

                OutputPacketBuffer[6] = MaxForce_Bytes[0];
				OutputPacketBuffer[7] = MaxForce_Bytes[1];
				OutputPacketBuffer[8] = MaxForce_Bytes[2];
				OutputPacketBuffer[9] = MaxForce_Bytes[3];

				OutputPacketBuffer[10] = Fun_a_Bytes[0];
				OutputPacketBuffer[11] = Fun_a_Bytes[1];
				OutputPacketBuffer[12] = Fun_a_Bytes[2];
				OutputPacketBuffer[13] = Fun_a_Bytes[3];

                OutputPacketBuffer[14] = Fun_b_Bytes[0];
				OutputPacketBuffer[15] = Fun_b_Bytes[1];
				OutputPacketBuffer[16] = Fun_b_Bytes[2];
				OutputPacketBuffer[17] = Fun_b_Bytes[3];


				WriteFile(WriteHandle, &OutputPacketBuffer, 65, &BytesWritten, 0);	//Blocking function, unless an "overlapped" structure is used

				timeBeginPeriod(1);
				FTimerID_Control = timeSetEvent(uDelay, uResolution, TimeProcControl, dwUser, fuEvent);

				return 1;
			}

		}

		InterfaceIndex++;	
	}
}


void SetForce(float Force_XScale,float Force_YScale, int duration)
{
	isSetDutyOnCh=false;

	if(Force_XScale>1.0) Force_XScale=1.0;
    if(Force_YScale>1.0) Force_YScale=1.0;

	float Force_X,Force_Y;
	Force_X=Force_XScale*Max_Force;
    Force_Y=Force_YScale*Max_Force;
	SetForce_Mag=sqrt(Force_X*Force_X+Force_Y*Force_Y);
	if (SetForce_Mag>0.0) {

		SetForce_Dirunit_X=Force_X/SetForce_Mag;
		SetForce_Dirunit_Y=Force_Y/SetForce_Mag;
	}
	if (duration<=0)
	{
		continuousFlag=true;
	}
	else
	{
		continuousFlag=false;
		impulseFlag = true;
		nowTime = 0;
		waitTime=duration;
	}
}
bool CloseSpidarMouse() 
{

	 DWORD BytesWritten = 0;
	 unsigned char OutputPacketBuffer[65];	
	 OutputPacketBuffer[0] = 0;			
	 OutputPacketBuffer[1] = 0x99;		//stop pic control 
	 WriteFile(WriteHandle, &OutputPacketBuffer, 65, &BytesWritten, 0);
	 return true;
}

void SetMinForceDuty(float MinForceDuty)
{
	     
	 unsigned char MinForce_Duty;

	 if ((MinForceDuty>=0)&&(MinForceDuty<=1.0))
	 {
		 MinForce_Duty=MinForceDuty*100;//transfered to percentage
	 }
	 else if(MinForceDuty>1.0)
	 {
		 MinForce_Duty=100;
	 }
	 else
	 {
		 MinForce_Duty=0;
	 }
	            
	 OutputPacketBuffer[0] = 0;	
		
     OutputPacketBuffer[1] = 0x94;		 
	
	 OutputPacketBuffer[2] = MinForce_Duty;

	 WriteFile(WriteHandle, &OutputPacketBuffer, 65, &BytesWritten, 0);
}
void SetDutyOnCh(float duty1, float duty2, float duty3, float duty4, int duration)
{
	 
	 isSetDutyOnCh=true;

	 if ((duty1>=0)&&(duty1<=1.0))
	 {
		 duty1value=duty1*100;//transfered to percentage
	 }
	 else if(duty1>1.0)
	 {
		 duty1value=100;
	 }
	 else
	 {
		 duty1value=0;
	 }

	 if ((duty2>=0)&&(duty2<=1.0))
	 {
		 duty2value=duty2*100;//transfered to percentage;
	 }
	 else if(duty2>1.0)
	 {
		 duty2value=100;
	 }
	 else
	 {
		 duty2value=0;
	 }

	 if ((duty3>=0)&&(duty3<=100))
	 {
		 duty3value=duty3*100;//transfered to percentage;
	 }
	 else if(duty3>1.0)
	 {
		 duty3value=100;
	 }
	 else
	 {
		 duty3value=0;
	 }

	 if ((duty4>=0)&&(duty4<=100))
	 {
		 duty4value=duty4*100;//transfered to percentage;
	 }
	 else if(duty4>1.0)
	 {
		 duty4value=100;
	 }
	 else
	 {
		 duty4value=0;
	 }

	 if (duration<=0)
	 {
		continuousFlag=true;
	 }
	 else
	 {
		continuousFlag=false;
		impulseFlag = true;
		nowTime = 0;
		waitTime=duration;
	 }

	 //OutputPacketBuffer[0] = 0;	
		//
  //   OutputPacketBuffer[1] = 0x93;		 
	
	 //OutputPacketBuffer[2] = duty1value;
	 //OutputPacketBuffer[3] = duty2value;
	 //OutputPacketBuffer[4] = duty3value;
	 //OutputPacketBuffer[5] = duty4value;

	 //WriteFile(WriteHandle, &OutputPacketBuffer, 65, &BytesWritten, 0);

}
bool substr(const wchar_t* s,const wchar_t* t) 
{ 
	typedef unsigned int uint; 
	//using typedef to define unsigned int��otherwise replace the uint in the following code with unsigned int 
	uint s_size = wcslen(s),t_size = wcslen(t); 
	if (s_size>=t_size)
	{
		uint i=0; 
		for( ;i<s_size-t_size+1;++i ) 
		  { 
			bool ok = true; 
			for( uint j=0;j<t_size; ++j ) 
			{ 
				if( toupper(s[i+j]) != toupper(t[j]) ) 
				{ 
					ok = false; 
					break; 
				} 
			} 
			if( ok ) 
			   return true; 
			} 
			return false; 
	} 
	else 
		return false;
}
