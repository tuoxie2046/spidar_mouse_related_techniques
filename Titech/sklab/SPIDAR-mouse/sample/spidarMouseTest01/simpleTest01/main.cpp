#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <SpidarMouse.h> 

#pragma comment(lib, "SpidarMouse.lib") 

int main(int argc, char* argv[])
{    
	
	printf("SpidarMouseを初期化します。\n");
	int ret = StartSpidarMouse();
	if (ret > 0)    
	{        
		printf("Enterキーを押してください。1秒間力を提示します。\n");
		getchar();
		SetImpulse(0, 2, 1000);
		printf("Enterキーを押してください。5秒間力を提示します。\n");
		getchar();
		SetForce(0, 2);
		Sleep(5000);
		SetForce(0, 0);
		printf("Enterキーを押してください。5秒間力を提示します。\n");
		getchar();
		SetForce(0, -2);
		Sleep(5000);
		SetForce(0, 0);
		printf("Enterキーを押してください。5秒間力を提示します。\n");
		getchar();
		SetForce(2, 0);
		Sleep(5000);
		SetForce(0, 0);
		printf("Enterキーを押してください。5秒間力を提示します。\n");
		getchar();
		SetForce(-2, 0);
		Sleep(5000);
		SetForce(0, 0);
		printf("Enterキーを押してください。終了します。\n");
		getchar();
		StopSpidarMouse();
	}
	else
	{
		printf("SpidarMouseの初期化に失敗しました。%d\n", ret);
		getchar();
		return 0;
	}
	return 0;
}
