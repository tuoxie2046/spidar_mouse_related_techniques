#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <SpidarMouse.h> 

#pragma comment(lib, "SpidarMouse.lib") 

int main(int argc, char* argv[])
{    
	float duty = 0.1;
	printf("SpidarMouseを初期化します。\n");
	int ret = OpenSpidarMouse();
	if (ret > 0)    
	{
        printf("Usage:\n");
        printf("wキー: SetForce( 0,  1, 1000)\n");
        printf("sキー: SetForce( 0, -1, 1000)\n");
        printf("aキー: SetForce( 1,  0, 1000)\n");
        printf("dキー: SetForce(-1,  0, 1000)\n");
        printf("zキー: SetMinForceDuty down\n");
        printf("xキー: SetMinForceDuty up\n");
        printf("ESCキー：終了\n");
        while(1)
        {
            if (kbhit())
            {
                int ch = getch();
                switch(ch)
                {
                case 0x1b:
            		CloseSpidarMouse();
                    return 0;
                case 'w':
                    SetForce(0, 1, 1000);
                    break;
                case 's':
                    SetForce(0, -1, 1000);
                    break;
                case 'a':
                    SetForce(1, 0, 1000);
                    break;
                case 'd':
                    SetForce(-1, 0, 1000);
                    break;
                case 'z':
                    duty -= 0.01;
                    if (duty < 0) duty = 0;
                    SetMinForceDuty(duty);
                    printf("duty=%f\n", duty);
                    break;
                case 'x':
                    duty += 0.01;
                    if (duty > 1.0) duty = 1.0;
                    SetMinForceDuty(duty);
                    printf("duty=%f\n", duty);
                    break;
                }
            }
        }
	}
	else
	{
		printf("SpidarMouseの初期化に失敗しました。%d\n", ret);
		getchar();
		return 0;
	}
	return 0;
}
