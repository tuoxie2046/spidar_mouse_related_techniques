// forked from clockmaker's code on 2008-12-17
// http://wonderfl.net
// MIT license
package 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import Spidar.Spidar2D.SpidarMouse;
	
	/**
	 * ボールを投げ遊ぶサンプル
	 */
	public class Main extends Sprite
	{
		private var ball:Sprite;
		private var vx:Number = 0;
		private var vy:Number = 0;
		private var oldX:Number;
		private var oldY:Number;
		private var flag:Boolean = false;
		public var deviceSPIDAR:SpidarMouse = new SpidarMouse("localhost", 8080);
		
		/**
		 * コンストラクター(実行時にはじめに実行される関数)
		 */
		public function Main()
		{
			// フレームレートを設定
			stage.frameRate = 60;
			
			//　ボールを作成
			ball = new Sprite();
			ball.graphics.beginFill(0x999999, 1);
			ball.graphics.drawCircle(0, 0, 50);
			this.addChild(ball);
			
			// インタラクティブの設定
			ball.addEventListener(MouseEvent.MOUSE_DOWN, downHandler);
			ball.addEventListener(MouseEvent.MOUSE_UP, upHandler);
			this.addEventListener(Event.ENTER_FRAME, enterHandler);
		}
		
		/**
		 * ボールを押したときの処理です
		 */
		private function downHandler(e:MouseEvent):void 
		{
			// マウスの位置を保存
			oldX = this.mouseX;
			oldY = this.mouseY;
			
			// ドラッグを開始
			ball.startDrag(true);
			
			// ボールの速度を無効にする
			flag = true;
			
			// 重力を提示する
			deviceSPIDAR.setForce(0.0, -1.5, -1);
		}
		
		/**
		 * ボールからマウスを離したときの処理です
		 */
		private function upHandler(e:MouseEvent):void 
		{
			// ボールの速度を有効にする(ドラッグした距離に応じて、速度を設定)
			vx = this.mouseX - oldX;
			vy = this.mouseY - oldY;
			
			// ドラッグを解除
			ball.stopDrag();
			
			// ボールの速度を無効にする
			flag = false;
			
			//重力を解放する
			deviceSPIDAR.setForce(0.0, 0.0, 0);
		}
		
		/**
		 * フレーム毎に実行される処理です(アニメーション用途で設定しています)
		 */
		private function enterHandler(e:Event):void 
		{
			// ボールをドラッグ中はアニメーションを無効化
			if (flag) return;
			
			//重力
			vy += .5; 
			
			// 摩擦
			vx *= 0.97; 
			vy *= 0.97;
			
			// ボールに物理演算を適用
			ball.x += vx; 
			ball.y += vy;
			
			// 画面の端からはみ出さないようにする処理
			if(ball.x + ball.width/2 > stage.stageWidth)
			{
				ball.x  = stage.stageWidth - ball.width / 2;
				vx *= -0.7;
				//壁衝突の力覚提示
				deviceSPIDAR.setForce(-1.0, 0.0, 100);
			}
			else if(ball.x - ball.width/2 < 0)
			{
				ball.x  = 0 + ball.width / 2;
				vx *= -0.7;
				//壁衝突の力覚提示
				deviceSPIDAR.setForce(1.0, 0.0, 100);
			}
			if(ball.y + ball.height / 2 > stage.stageHeight)
			{
				ball.y  = stage.stageHeight - ball.height / 2;
				vy *= -0.7;
				//壁衝突の力覚提示
				//deviceSPIDAR.setForce(0.0, 0.0, 0);
			}
			else if(ball.y - ball.height / 2 < 0)
			{
				ball.y  = + ball.height / 2;
				vy *= -0.7;
				//壁衝突の力覚提示
				deviceSPIDAR.setForce(0.0, 1.5, 100);
			}
		}
	}
}
