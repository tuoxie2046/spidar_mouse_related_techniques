﻿package  
{
	import Box2D.Dynamics.b2Body;
	import flash.display.Sprite;
	/**
	 * ...
	 * @author mi
	 */
	public class ObjectData
	{
		// 画像表示用クラス
		public var sprite:Sprite;
		// 得点
		public var score:Number;
		// 体力
		public var hp:Number;
		// 種類
		public var type:Number;
		
		// 初期化
		public function ObjectData() 
		{
			score = 100;
			hp = 1;
			sprite = null;
			type = 0;
		}
		
	}

}