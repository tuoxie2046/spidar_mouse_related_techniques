﻿package 
{
	import Box2D.Collision.b2AABB;
	import Box2D.Collision.b2Bound;
	import Box2D.Collision.Shapes.b2CircleDef;
	import Box2D.Collision.Shapes.b2CircleShape;
	import Box2D.Collision.Shapes.b2PolygonDef;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import Box2D.Dynamics.b2DebugDraw;
	import Box2D.Dynamics.b2World;
	import flash.display.*;
	import flash.events.*;
	import flash.text.*;
	import flash.utils.*;
	import caurina.transitions.Tweener;
	import Spidar.Spidar2D.SpidarMouse;
	import Box2D.Collision.b2ContactPoint;
	import Box2D.Dynamics.b2ContactListener;
	
	/**
	 * ...
	 * @author Demo
	 */
	public class Main extends Sprite 
	{
		private var world:b2World;						// box2d world
		// Spidar-mouse device
//		public var device:SpidarMouse = new SpidarMouse("192.168.138.131", 8080);
		public var device:SpidarMouse = new SpidarMouse("127.0.0.1", 8080);
		private var offset:Array = [ 3.2, 1.0 ];		// ballの位置
		public var sphereBody:b2Body = null;			// ballのBody
		public var playerBody:b2Body = null;			// ユーザが動かすバーのBody
		private var blockNum:Number;					// ブロックの個数
		private var pos:b2Vec2 = new b2Vec2(0, 0);		// バーの位置
		private var impulse:b2Vec2 = new b2Vec2(0, 0);	// バーに加える力
		private var center:b2Vec2 = new b2Vec2(0, 0);	// 力を加える中心位置
		private var targetPos:b2Vec2 = new b2Vec2(0, 0);// 目標座標（マウスの位置）
		private var tf:TextField = new TextField();		// 文字列表示用
		private var angle:Number = new Number(0);		// ボールの回転角度
		private var rotVel:Number = new Number(0);		// ボールの回転速度
		private var lastFrameTime:Number;				// 前回のフレームの時刻
		private var maxSpeed:Number = 3.5;				// ボールの最高速度
		public static const DRAW_SCALE:Number = 100;	// 物理エンジン内の1mを表すためのピクセル数
		// 画像の組み込み
		[Embed(source='star.png')]
		private static const StarImage:Class;
		[Embed(source='beer01.gif')]
		private static const block1Image:Class;
		[Embed(source='coffee01.gif')]
		private static const block2Image:Class;
		[Embed(source = 'coke01.gif')]
		private static const block3Image:Class;
		[Embed(source = 'juce01.gif')]
		private static const block4Image:Class;
		[Embed(source = 'tea01.gif')]
		private static const block5Image:Class;
		[Embed(source = 'bna01.gif')]
		private static const playerImage:Class;
		[Embed(source = 'floor.png')]
		private static const floorImage:Class;
		[Embed(source = 'ball.png')]
		private static const ballImage:Class;

		// メイン関数
		public function Main():void 
		{
			// 初期化
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		// 初期化関数
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			// テキストの初期設定
			tf.autoSize = TextFieldAutoSize.LEFT;
			tf.text = "Hello world!\n";
			tf.x = 20;
			tf.y = 20;
			addChild(tf);
			// フレームレートの設定
			stage.frameRate = 40;
			// イベントハンドラを登録する
			stage.addEventListener(MouseEvent.MOUSE_MOVE, moveHandler);
			stage.addEventListener(MouseEvent.CLICK, clickHandler);
			addEventListener(Event.ENTER_FRAME, enterFrameHandler);
			// box2d worldの作成
			initPhysics();
		}

		// オブジェクトの作成
		private function createObject(mass:Number, posX:Number, posY:Number,
			sizeX:Number, sizeY:Number, angle:Number, type:Number, hp:Number, 
			score:Number, tex:Bitmap): b2Body {
			// Objectの位置をposX, posYとする
			var tempBodyDef:b2BodyDef = new b2BodyDef();
			tempBodyDef.position.Set(posX, posY);
			// 回転角度がある場合は設定
			tempBodyDef.angle = angle / 180.0 * Math.PI;
			// 物体は回転しないように設定
			tempBodyDef.fixedRotation = true;
			
			// Objectの形を、幅sizeX、厚さsizeYとする
			// 指定するのはその半分の値
			var tempShapeDef:b2PolygonDef = new b2PolygonDef();
			tempShapeDef.SetAsBox(sizeX / 2.0, sizeY / 2.0);
			// 質量設定
			tempShapeDef.density = mass;
			// 摩擦力設定
			tempShapeDef.friction = 0.0001;
			if (type == 2)
			{
				// バーの場合は反発係数を1.5に設定
				tempShapeDef.restitution = 1.5;
			}
			else
			{
				// それ以外の場合は1.0に設定
				tempShapeDef.restitution = 1.0;
			}
			
			// Objectの質量が0の場合は動かない物体として作る
			var tempBody:b2Body = world.CreateBody(tempBodyDef);
			tempBody.CreateShape(tempShapeDef);
			// Objectの質量が0より大きい場合は動く物体として作る
			if (mass > 0.0)
			{
				tempBody.SetMassFromShapes();
			}
			
			// テクスチャ画像がある場合は張る
			if (tex != null)
			{
				// 画像を読み込んで、サイズと位置を調整する
				tex.width = sizeX * DRAW_SCALE + 2;
				tex.height = sizeY * DRAW_SCALE + 2;
				tex.x = -tex.width / 2;
				tex.y = -tex.height / 2;
				
				// オブジェクトデータを保持するためのクラスを作る
				// box2dのUserDataとして作成
				tempBody.m_userData = new ObjectData();
				// 画像を表示するためのSpriteクラスを作成
				tempBody.GetUserData().sprite = new Sprite();
				tempBody.GetUserData().sprite.x = posX * DRAW_SCALE;
				tempBody.GetUserData().sprite.y = posY * DRAW_SCALE;
				tempBody.GetUserData().sprite.addChild(tex);
				// オブジェクトのデータを設定
				tempBody.GetUserData().hp = hp;
				tempBody.GetUserData().score = score;
				tempBody.GetUserData().type = type;
				addChild(tempBody.GetUserData().sprite);
			}
			
			// 作成したb2Bodyを返す
			return tempBody;
		}

		// ボールの作成
		private function createBall(): b2Body {
			// ボールの作成
			var sphereBodyDef:b2BodyDef = new b2BodyDef();
			sphereBodyDef.position.Set(offset[0], offset[1]);
			sphereBodyDef.angularDamping = 0.0;
			sphereBodyDef.allowSleep = false;
			var sphereShapeDef:b2CircleDef = new b2CircleDef();
			sphereShapeDef.density = 0.001;
			sphereShapeDef.radius = 0.1;
			sphereShapeDef.restitution = 0.8;
			sphereShapeDef.friction = 0.000001;
			var tempBody:b2Body = world.CreateBody(sphereBodyDef);
			tempBody.CreateShape(sphereShapeDef);
			tempBody.SetMassFromShapes();
			// ボール画像の生成
			var ball:Bitmap = new ballImage();
			// 画像を読み込んで、サイズと位置を調整する
			ball.width = 0.2 * DRAW_SCALE + 2;
			ball.height = 0.2 * DRAW_SCALE + 2;
			ball.x = -ball.width / 2;
			ball.y = -ball.height / 2;
			// オブジェクトデータの作成と設定
			tempBody.m_userData = new ObjectData();
			tempBody.GetUserData().sprite = new Sprite();
			tempBody.GetUserData().sprite.x = offset[0] * DRAW_SCALE;
			tempBody.GetUserData().sprite.y = offset[1] * DRAW_SCALE;
			tempBody.GetUserData().sprite.addChild(ball);
			tempBody.GetUserData().hp = 1;
			tempBody.GetUserData().score = 0;
			tempBody.GetUserData().type = 1;
			addChild(tempBody.GetUserData().sprite);
			// 作成したb2Bodyを返す
			return tempBody;
		}

		// box2dワールドの初期化
		private function initPhysics():void {
			////////////////////////////////////////
			// 物理エンジンのセットアップ
			// 外枠を定義する
			var worldAABB:b2AABB = new b2AABB();
			worldAABB.lowerBound.Set(-100, -100);
			worldAABB.upperBound.Set(100, 100);
			
			// 重力を右方向に0.1m/s^2とする
			var gravity:b2Vec2 = new b2Vec2(0.1, 0);
			
			// 外枠と重力を指定して、物理エンジン全体をセットアップする
			world = new b2World(worldAABB, gravity, true);
			
			// 衝突コールバッククラスの設定
			var contactListener:ContactListener = new ContactListener();
			contactListener.parent = this;
			world.SetContactListener(contactListener);
			
			////////////////////////////////////////
			// 床の設置
			var downBMP:Bitmap = new floorImage();
			createObject(0, 2.5, 3.5, 5.2, 0.2, 0, 0, 0, 0, downBMP);
			// 天井
			var upBMP:Bitmap = new floorImage();
			createObject(0, 2.5, 0, 5.2, 0.2, 0, 0, 0, 0, upBMP);
			// 左壁
			var leftBMP:Bitmap = new floorImage();
			createObject(0, 0, 1.76, 3.68, 0.2, 90, 0, 0, 0, leftBMP);
			// 右壁
			var rightBMP:Bitmap = new floorImage();
			createObject(0, 5.0, 1.76, 3.68, 0.2, 90, 5, 0, 0, rightBMP);
						
			// ボール作成
			sphereBody = createBall();
			impulse.x = 0.0001;
			impulse.y = 0.0001;
			center.x = 0.0;
			center.y = 0.0;
			sphereBody.ApplyForce(impulse, center);
				
			// ブロック作成
			blockNum = 0;
			for (var i:Number = 0.6; i <= 3.0; i += 0.2) {
				// ブロック画像の生成
				var block1:Bitmap = new block1Image();
				var block2:Bitmap = new block2Image();
				var block3:Bitmap = new block3Image();
				var block4:Bitmap = new block4Image();
				var block5:Bitmap = new block5Image();
				// ブロック作成
				createObject(0, i, 0.8, 0.2, 0.4, 0, 3, 5, 500, block1);
				createObject(0, i, 1.2, 0.2, 0.4, 0, 3, 1, 100, block2);
				createObject(0, i, 1.6, 0.2, 0.4, 0, 3, 3, 300, block3);
				createObject(0, i, 2.0, 0.2, 0.4, 0, 3, 1, 100, block4);
				createObject(0, i, 2.4, 0.2, 0.4, 0, 3, 5, 500, block5);
				blockNum += 5;
			}
			
			// Player操作用バーを作成する
			var player:Bitmap = new playerImage();
			playerBody = createObject(1.0, 4.7, 1.0, 0.5, 0.1, 90, 2, 10, 0, player);
			
			////////////////////////////////////////
			// 描画設定
			var debugDraw:b2DebugDraw = new b2DebugDraw();
			debugDraw.m_sprite = this;
			debugDraw.m_drawScale = 100; // 1mを100ピクセルにする
			debugDraw.m_fillAlpha = 0.01; // 不透明度
			debugDraw.m_lineThickness = 0.1; // 線の太さ
			debugDraw.m_drawFlags = b2DebugDraw.e_shapeBit;
			world.SetDebugDraw(debugDraw);				
		}
		
		// 新しく衝突が発生したときに呼び出されるメソッド
		public function addContactEffect(point:b2ContactPoint):void {
			// 衝突が発生したときに3個の星を飛ばす
			for (var i:int = 0; i < 3; ++i) {
				// 星の画像を読み込んで、サイズと位置を調整する
				var starImage:Bitmap = new StarImage();
				starImage.width = 50;
				starImage.height = 50;
				starImage.x = -starImage.width / 2;
				starImage.y = -starImage.height / 2;
				
				// 星の画像を表示するためのSpriteを作る
				// 座標はコンタクトが発生した場所とする
				var sprite:Sprite = new Sprite();
				sprite.x = point.position.x * DRAW_SCALE;
				sprite.y = point.position.y * DRAW_SCALE;
				sprite.addChild(starImage);
				addChild(sprite);
				
				// 星が飛んでいく方向と距離をランダムに決める
				// 角度は0～360度、距離は50～150
				var angle:Number = Math.random() * 360;
				var length:Number = Math.random() * 100 + 50;
				// 星が飛んでいく先の座標を計算する
				var dx:Number = sprite.x + length * Math.cos(angle);
				var dy:Number = sprite.y + length * Math.sin(angle);
				
				// Tweenerで星を飛ばす
				Tweener.addTween(sprite, {
					time: 3, // 3秒間
					scaleX: 0, // 消えるまで縮小する
					scaleY: 0,
					alpha: 0, // 完全に透明にする
					rotation: 200, // 少し回転させる
					x: dx,
					y: dy,
					onComplete: function():void {
						removeChild(sprite);
					}
				});
			}

			// バーが何かにぶつかった場合に力を発生させる
			if (point.shape1.GetBody() == playerBody)
			{
				device.setForce(-1.2*point.normal.x, -1.2*point.normal.y, 50);
			}
			else if (point.shape2.GetBody() == playerBody)
			{
				device.setForce(1.2*point.normal.x, -1.2*point.normal.y, 50);
			}

			// ボールが何かにぶつかった場合の処理
			if (point.shape1.GetBody() == sphereBody)
			{
				// ブロックにぶつかった場合
				if (point.shape2.GetBody().GetUserData().type == 3)
				{
					point.shape2.GetBody().GetUserData().hp--;
				}
				// 右壁にぶつかった場合
				else if (point.shape2.GetBody().GetUserData().type == 5)
				{
					point.shape1.GetBody().GetUserData().hp--;
				}
			}
			else if (point.shape2.GetBody() == sphereBody)
			{
				// ブロックにぶつかった場合
				if (point.shape1.GetBody().GetUserData().type == 3)
				{
					point.shape1.GetBody().GetUserData().hp--;
				}		
				// 右壁にぶつかった場合
				else if (point.shape1.GetBody().GetUserData().type == 5)
				{
					point.shape2.GetBody().GetUserData().hp--;
				}
			}
		}
		
		// マウスカーソルが移動した場合、目標座標を更新
		private function moveHandler(event:MouseEvent):void {
			targetPos.x = event.stageX;
			targetPos.y = event.stageY;
		}

		// 左クリックした場合の処理
		private function clickHandler(event:MouseEvent):void {
			// ボールが存在しない場合
			if (sphereBody == null)
			{
				// ボールを作成
				sphereBody = createBall();
			}
			// ボールが存在する場合
			if (sphereBody != null)
			{
				// 右下方向に力を加える
				impulse.x = 0.005;
				impulse.y = 0.005;
				center.x = 0.0;
				center.y = 0.0;
				sphereBody.ApplyForce(impulse, center);
			}
		}
		
		// 設定したフレームレート毎に呼び出される関数
		private function enterFrameHandler(event:Event):void {
			// box2dのワールドが存在しない場合は何もしない
			if (world == null) {
				return;
			}
			
			// ブロックが存在しない場合はゲームクリア
			if (blockNum <= 0)
			{
				// 得点表示
				if (blockNum == 0)
				{
					playerBody.GetUserData().score += playerBody.GetUserData().hp * 10000;
				}
				tf.text = "Game Clear!\n" + "Score : " + playerBody.GetUserData().score;
				addChild(tf);
				blockNum--;
				return;
			}

			// ボール数が0になった場合はゲーム終了
			if (playerBody.GetUserData().hp <= 0)
			{
				tf.text = "Game Over!\n" + "Score : " + playerBody.GetUserData().score;
				addChild(tf);
				return;				
			}

			// FPSの計測
			var now:Number = getTimer();
			var elapsed:Number = now - lastFrameTime;
			var framesPerSecond:Number = Math.round(1000 / elapsed);
//				tf.text = "elapsed: " + elapsed
//					+ "\nreal_fps: " + framesPerSecond
//					+ "\nset_fps: " + stage.frameRate;
			lastFrameTime = now;

			// ボールが存在する場合
			if (playerBody != null && sphereBody != null)
			{
				
				// 設定したFPSで物理シミュレーションを進める
//				world.Step(1.0 / framesPerSecond, 10);
//				world.Step(1.0 / 30.0, 10);
				world.Step(1.0 / stage.frameRate, 10);
				
				// ボールの速度制限
				var length:Number = sphereBody.GetLinearVelocity().Length();
				if (length > maxSpeed)
				{
					var normalVel:b2Vec2 = new b2Vec2();
					normalVel.x = sphereBody.GetLinearVelocity().x / length * maxSpeed;
					normalVel.y = sphereBody.GetLinearVelocity().y / length * maxSpeed;
					sphereBody.SetLinearVelocity(normalVel);
				}
				
				// バーとマウスのY座標をバーチャルカップリング
				pos = playerBody.GetPosition();
				center.x = 4.7;
				center.y = targetPos.y / 100.0;
				// バーの移動範囲制限
				if (center.y < 0.4)
				{
					center.y = 0.4;
				}
				else if (center.y > 3.1)
				{
					center.y = 3.1;
				}
				// バーチャルカップリングに使用する力の計算
				impulse.x = 50.0*(center.x - pos.x) - 1.5*playerBody.GetLinearVelocity().x;
				impulse.y = 50.0*(center.y - pos.y) - 1.5*playerBody.GetLinearVelocity().y;
				center.x = 0;
				center.y = 0;
				// バーに力を加える
				playerBody.ApplyForce(impulse, center);

				// ワールド内の全てのb2Bodyに対する処理
				for (var b:b2Body = world.GetBodyList(); b; b = b.GetNext()) {
					if (b.GetUserData() is ObjectData) {
						// 物理エンジン内での位置と回転角度を反映させる
						b.GetUserData().sprite.x = b.GetWorldCenter().x * DRAW_SCALE;
						b.GetUserData().sprite.y = b.GetWorldCenter().y * DRAW_SCALE;
						b.GetUserData().sprite.rotation = b.GetAngle() * 180 / Math.PI;
					}
				}

				// ワールド内の全てのb2Bodyに対する処理
				var b2:b2Body = world.GetBodyList();
				var nextb:b2Body;
				while (b2 != null) {
					nextb = b2.GetNext();
					if (b2.GetUserData() is ObjectData) {
						// HPが0のブロックは削除する
						if (b2.GetUserData().hp <= 0 && b2.GetUserData().type == 3)
						{
							// プレイヤのスコアにブロックのスコアを加える
							playerBody.GetUserData().score += b2.GetUserData().score;
							// ブロックの画像を削除
							removeChild(b2.GetUserData().sprite);
							// ブロックのBodyを削除
							world.DestroyBody(b2);
							// 残りブロック数の更新
							blockNum--;
						}
						// ボールが右壁に当たった場合、ボールを削除する
						else if (b2.GetUserData().hp <= 0 && b2.GetUserData().type == 1)
						{
							// プレイヤのボール数を1つ減らす
							playerBody.GetUserData().hp--;
							// ボール画像の削除
							removeChild(b2.GetUserData().sprite);
							// ボールのBodyを削除
							world.DestroyBody(b2);
							sphereBody = null;
						}
					}
					b2 = nextb;
				}
				
				// 現在の情報表示
				tf.text = "fps : " + framesPerSecond + "\n" + 
					"ball : " + playerBody.GetUserData().hp + "\n" + "Score : " + playerBody.GetUserData().score;
				addChild(tf);
			}
		}		
	}
	
}