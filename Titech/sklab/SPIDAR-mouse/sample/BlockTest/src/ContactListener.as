﻿package  {
	import Box2D.Collision.b2ContactPoint;
	import Box2D.Dynamics.b2ContactListener;

	// 衝突時のコールバック関数を扱うクラス
	public class ContactListener extends b2ContactListener {
		public var parent:Main
		
		public function ContactListener() {
		}
		
		// 新しく衝突が発生したときに呼び出されるメソッド
		public override function Add(point:b2ContactPoint):void {
			// 衝突があった場合親クラスの関数を呼び出す
			parent.addContactEffect(point);
		}
		
		// 衝突中に呼び出されるメソッド
		public override function Persist(point:b2ContactPoint):void {
//				parent.device.setForce(0, 0, 0);
		}
		
		// 衝突がなくなったときに呼び出されるメソッド
		public override function Remove(point:b2ContactPoint):void {
			// バーとの接触がなくなった場合力の出力を0Nにする
//			if (point.shape1.GetBody() == parent.playerBody ||
//				point.shape2.GetBody() == parent.playerBody)
//			{
	//			parent.device.setForce(0, 0, 0);
//			}
//				parent.device.setForce(0, 0, 0);
		}
	}
}