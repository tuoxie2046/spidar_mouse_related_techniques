//start Spidar Mouse device
extern "C" _declspec(dllexport) int StartSpidarMouse();

//set minimum force of string
extern "C" _declspec(dllexport) void SetMinForce(float MinForce);

//set maximum force of string
extern "C" _declspec(dllexport) void SetMaxForce(float MaxForce);

//set output force of device
extern "C" _declspec(dllexport) void SetForce(float Force_X, float Force_Y);

//set output force scale of device(from 0 to 1.0) and last for ms millisecond
extern "C" _declspec(dllexport) void SetImpulse(float Force_XScale,float Force_YScale, int ms);

//stop Spidar Mouse device
extern "C" _declspec(dllexport) bool StopSpidarMouse();


//used to deal with the string
bool substr( const wchar_t* s,const wchar_t* t );