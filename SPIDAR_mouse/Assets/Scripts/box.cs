﻿using UnityEngine;
using System.Collections;

public class box : MonoBehaviour 
{
	private Vector3 _screenPoint;
	private Vector3 _offset;
	private Vector2 _spidarForce; 
	private bool	_nowCollision;
	//以下は仮想のバネダンパ係数である．
	//Kを大きくすると追随性能が良くなるが，物体が振動しやすくなる
	//Bを大きくすると追随性能は落ちるが，物体の振動がより速く収束しやすくなる
	//以下2つの係数は適切に変更されたし．
	public float VirtualK=5.0f;
	public float VirtualB=0.5f;
	
	void Start()
	{
		SpidarMouse.Instance.SetForce2D( 0.0f, 0.0f );
		_spidarForce.Set( 0.0f, 0.0f );
		_nowCollision = false;
	}
	
	void Update()
	{
		if ( _nowCollision )
		{
			SpidarMouse.Instance.SetForce2D( _spidarForce.x, _spidarForce.y );
			Debug.Log( _spidarForce );
		}
		else
		{
			SpidarMouse.Instance.SetForce2D( 0.0f, 0.0f );
			Debug.Log("Spidar-mouse exit");
		}
	}
	
	void OnMouseDown() 
	{
		_screenPoint	= Camera.main.WorldToScreenPoint( transform.position );
		_offset			= transform.position - Camera.main.ScreenToWorldPoint( new Vector3( Input.mousePosition.x, Input.mousePosition.y, _screenPoint.z) );
	}
	
	void OnMouseDrag()
	{
		Vector3 currentScreenPoint	= new Vector3( Input.mousePosition.x, Input.mousePosition.y, _screenPoint.z );
		Vector3 currentPosition		= Camera.main.ScreenToWorldPoint( currentScreenPoint ) + _offset;
		Vector3 VCK=VirtualK*(currentPosition - transform.position);
		Vector3 VCB=VirtualB*(this.GetComponent<Rigidbody>().velocity);
		//rigidbody.AddForce( (currentPosition - transform.position), ForceMode.VelocityChange );
		GetComponent<Rigidbody>().AddForce(VCK-VCB, ForceMode.VelocityChange );
		Vector3 force = - GetComponent<Rigidbody>().mass * ( currentPosition - transform.position );
		_spidarForce.Set( force.x, -force.z );
	}
	
	void OnCollisionEnter( Collision collision )
	{
		_nowCollision = true;
	}
	
	void OnCollisionExit( Collision collision)
	{
		_nowCollision = false;
	}
}
